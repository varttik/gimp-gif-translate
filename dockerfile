FROM fedora:28

# install GIMP
RUN dnf -y update && \
  dnf install -y dnf-plugins-core  && \
  dnf install -y fedora-repos-modular && \
  dnf module install -y gimp:2.10

# install AnimStack
RUN curl -sSLO https://github.com/tshatrov/scriptfu/releases/download/animstack-0.62/animstack.scm && \
    mv animstack.scm /usr/share/gimp/2.0/scripts/animstack.scm

# install fonts
COPY LearntoFly.otf /usr/share/fonts/learntofly/LearntoFly.otf
RUN fc-cache -v

# install python libs
RUN pip install PyYaml

# Add scripts
COPY translate.sh /translate.sh
RUN chmod +x /*.sh

RUN dnf install -y smc-*-fonts vemana2000-fonts.noarch

COPY translate-gif.py /translate-gif.py




