#!/usr/bin/python
 
import gimpfu
import math
import os
import sys

from sys import platform
if platform == "linux" or platform == "linux2":
  import yaml
elif platform == "darwin":
  # osx hack for yaml
  sys.path.append('/usr/local/lib/python2.7/site-packages')
  import yaml
elif platform == "win32":
  import yaml

def getBaseName(templatePath):
    templateFile = os.path.basename(templatePath)
    baseName = templateFile.rsplit(".",1)[0]
    return baseName 

def getYamlConfig(file):
    with open(file) as f:
        config = yaml.load(f)
    return config

def getJustification(justify):
    if justify == 'left':
      return TEXT_JUSTIFY_LEFT
    if justify == 'center':
      return TEXT_JUSTIFY_CENTER
    if justify == 'right':
      return TEXT_JUSTIFY_RIGHT
    else:
      return TEXT_JUSTIFY_LEFT

def mergeText(image, layer, text, size, id='', xOffset=0, yOffset=0, width=100, hight=100, rotate=0, justify='left', font='Learn to Fly'):
    layer.visible = True

    # Get size of text
    if debug:
        print("template box %i/%i") % (width, hight)

    textExtents = pdb.gimp_text_get_extents_fontname(text, size, PIXELS, font)
    textLayerWidth = textExtents[0]
    textLayerHight = textExtents[1]
    if debug:
        print("textbox %i/%i") % (textLayerWidth, textLayerHight)
    
    if justify == 'left':
      x_margin = 0

    if justify == 'right':
      x_margin = (width - textLayerWidth)

    if justify == 'center':
      x_margin = (width - textLayerWidth) / 2

    y_margin = (hight - textLayerHight) / 2

    if debug:
        print("margin %i/%i") % (x_margin, x_margin)
    
    new_x_offset = (xOffset + x_margin)
    new_y_margin = (yOffset + y_margin)
    if debug:
        print("new offsets %i/%i") % (new_x_offset, new_y_margin)

    textLayer = pdb.gimp_text_fontname(image, layer, new_x_offset, new_y_margin, text, 0, True, size, PIXELS, font)

    if justify != 'left':
      pdb.gimp_text_layer_set_justification(textLayer, getJustification(justify))
    
    if rotate != 0:
      pdb.gimp_item_transform_rotate(textLayer, math.radians(rotate), False, xOffset, yOffset)
    
    mergedLayer = pdb.gimp_image_merge_visible_layers(image, 0)
    mergedLayer.visible = False
    return mergedLayer

def addText(layerConfigs, filename):
    img = pdb.gimp_file_load(filename, filename)
    texts = getYamlConfig(os.path.join(languageDir, i18n, textFileName))

    for layerConfig in layerConfigs:      
        activeLayer = None
        for layer in img.layers:
            layer.visible = False
            if layer.name.startswith(layerConfig['name']):
                activeLayer = layer

        text_ids = []
        for text in layerConfig['texts']:
          text_ids.append(text['id'])
    
        for key, textValue in texts.iteritems():
            if key in text_ids:
                index = next((index for (index, d) in enumerate(layerConfig['texts']) if d["id"] == key), None)
                if debug:
                    print(key)
                    print(textValue)
                    print(layerConfig['texts'][index])
                # merge values (textValues will overwrite existing keys in layerConfig['texts'][0])
                mergedValue = dict(layerConfig['texts'][index].items() + textValue.items())
                activeLayer = mergeText(
                    img,
                    activeLayer,
                    **mergedValue
                )
    #image, layer, text, size, xOffset=0, yOffset=0, width=100, hight=100, rotate=0, justify='left', font='Learn to Fly'):
    
    # save the i18n version
    baseName = getBaseName(filename)
    i18nName = cacheDir + "/" + baseName + "-" + i18n + ".xcf"
    pdb.gimp_xcf_save(0, img, None, i18nName, i18nName)
    pdb.gimp_image_delete(img)
    return i18nName

def export(filename):
    img = pdb.gimp_file_load(filename, filename)
    if getYamlConfig(os.path.join(templateDir, baseFileName))['process_animstack']:
        pdb.script_fu_animstack_process_all(img)
        pdb.script_fu_flatten_layer_groups(img)
    pdb.gimp_image_convert_indexed(img, NO_DITHER, MAKE_PALETTE, 128, False, True, "")

    baseName = getBaseName(filename)

    outName = cacheDir + "/" + baseName + "-" + "out" + ".xcf" 
    pdb.gimp_xcf_save(0, img, None, outName, outName)

    gifName = os.path.join(releaseDir, baseName + ".gif")
    print("Generating " + gifName)
    pdb.file_gif_save(img, None, gifName ,gifName, 0, 1, 200 , 0)
    pdb.gimp_image_delete(img)
    return gifName

# get params from env
i18n = os.environ['I18N']
templateFile = os.environ['XCFFILE']

# config filename
textFileName = 'text.yaml'
layersFileName = 'layers.yaml'
baseFileName = 'base.yaml'

# set dirs
cacheDir = '.cache'
templateDir = 'templates'
languageDir = 'languages'
releaseDir = 'releases'

debug = getYamlConfig(os.path.join(templateDir, baseFileName))['debug']
layerConfigs = getYamlConfig(os.path.join(templateDir, layersFileName))

if not os.path.exists(cacheDir):
  os.mkdir(cacheDir)

i18nFilePath = addText(layerConfigs, templateFile)
gifFilePath = export(i18nFilePath)

pdb.gimp_quit(1)